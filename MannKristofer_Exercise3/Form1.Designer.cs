﻿namespace MannKristofer_Exercise3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.xoControl1 = new MannKristofer_Exercise3.xoControl();
            this.boardControl1 = new MannKristofer_Exercise3.BoardControl();
            this.SuspendLayout();
            // 
            // xoControl1
            // 
            this.xoControl1.Image = ((System.Drawing.Bitmap)(resources.GetObject("xoControl1.Image")));
            this.xoControl1.Location = new System.Drawing.Point(154, 413);
            this.xoControl1.Name = "xoControl1";
            this.xoControl1.SelectTile = new System.Drawing.Point(0, 0);
            this.xoControl1.Size = new System.Drawing.Size(345, 153);
            this.xoControl1.TabIndex = 5;
            this.xoControl1.Text = "xoControl1";
            this.xoControl1.XSize = new System.Drawing.Size(32, 32);
            this.xoControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.xoControl1_MouseClick);
            // 
            // boardControl1
            // 
            this.boardControl1.BoardImage = ((System.Drawing.Bitmap)(resources.GetObject("boardControl1.BoardImage")));
            this.boardControl1.BoardSize = new System.Drawing.Size(3, 3);
            this.boardControl1.Location = new System.Drawing.Point(108, 13);
            this.boardControl1.Name = "boardControl1";
            this.boardControl1.Size = new System.Drawing.Size(500, 394);
            this.boardControl1.TabIndex = 4;
            this.boardControl1.Text = "boardControl1";
            this.boardControl1.TileSelect = new System.Drawing.Point(1, 1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 545);
            this.Controls.Add(this.xoControl1);
            this.Controls.Add(this.boardControl1);
            this.Name = "Form1";
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private BoardControl boardControl1;
        private xoControl xoControl1;
    }
}

