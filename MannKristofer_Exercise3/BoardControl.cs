﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MannKristofer_Exercise3
{
    public partial class BoardControl : ScrollableControl
    {
        Size boardTile = new Size(64, 64);

        Bitmap boardImage = Properties.Resources.xo;
        public Bitmap BoardImage
        {
            get { return boardImage; }
            set { boardImage = value; Invalidate(); }
        }

        Point tileSelect = new Point(1, 1);
        public Point TileSelect
        {
            get { return tileSelect; }
            set { tileSelect = value; }
        }

        Point[,] board = new Point[3, 3];
        public Size BoardSize
        {
            get { return new Size(board.GetLength(0), board.GetLength(1)); }
            set { board = new Point[value.Width, value.Height]; Invalidate(); }
        }

        public BoardControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Rectangle r1 = new Rectangle();
                    r1.X = i * boardTile.Width;
                    r1.Y = j * boardTile.Height;
                    r1.Size = boardTile;


                    Rectangle r2 = new Rectangle();
                    r2.X = board[i, j].X * boardTile.Width;
                    r2.Y = board[i, j].Y * boardTile.Height;
                    r2.Size = boardTile;

                    pe.Graphics.DrawImage(boardImage, r1, r2, GraphicsUnit.Pixel);

                    pe.Graphics.DrawRectangle(Pens.Black, r1);
                }
            }

            base.OnPaint(pe);
        }

        private void BoardControl_MouseClick_1(object sender, MouseEventArgs e)
        {
            Point location = e.Location;

            location.X -= AutoScrollPosition.X;
            location.Y -= AutoScrollPosition.Y;
            location.X = location.X / boardTile.Width;
            location.Y = location.Y / boardTile.Height;
            board[location.X, location.Y] = tileSelect;
            Invalidate();
        }
    }
}
