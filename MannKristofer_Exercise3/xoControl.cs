﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MannKristofer_Exercise3
{
    public partial class xoControl : ScrollableControl
    {
        public Size xSize = new Size(32, 32);
        public Size XSize
        {
            get { return xSize; }
            set { xSize = value; }
        }

        Bitmap image = Properties.Resources.xo;
        public Bitmap Image
        {
            get { return image; }
            set { image = value; Invalidate(); }
        }

        Point selectTile = new Point();
        public Point SelectTile
        {
            get { return selectTile; }
            set { selectTile = value; }
        }

        public void TileSelected(int x, int y)
        {
            selectTile = new Point(x, y);
            Invalidate();
        }

        Point[,] tiles = new Point[2, 1];

        public xoControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            pe.Graphics.DrawImage(image, this.AutoScrollPosition);

            for (int i = 1; i < tiles.GetLength(0); i++)
            {
                for (int j = 1; j < tiles.GetLength(1); j++)
                {

                    Rectangle r = Rectangle.Empty;
                    r.X = i * xSize.Width;
                    r.Y = j * xSize.Height;
                    r.Size = xSize;

                    pe.Graphics.DrawRectangle(Pens.Black, r);
                }
            }
            base.OnPaint(pe);
        }

    }
}
