﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MannKristofer_Exercise3
{
    public partial class Form1 : Form
    {
        public event EventHandler ChangeTile;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ChangeTile += HandlerChangeTile;
        }

        public void HandlerChangeTile(object sender, EventArgs e)
        {
            
            boardControl1.TileSelect = xoControl1.SelectTile;

        }

        private void xoControl1_MouseClick(object sender, MouseEventArgs e)
        {
            Point location = e.Location;
            location.X -= AutoScrollPosition.X;
            location.Y -= AutoScrollPosition.Y;
            if (xoControl1.XSize.Width > 0 && xoControl1.XSize.Height > 0)
            {
                location.X = location.X / xoControl1.XSize.Width;
                location.Y = location.Y / xoControl1.XSize.Height;
                xoControl1.SelectTile = location;
                xoControl1.TileSelected(location.X, location.Y);
            }
            if (ChangeTile != null)
            {
                ChangeTile(this, new EventArgs());
            }
        }
    }
}
